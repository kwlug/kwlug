Kitchener-Waterloo Linux User Group
===================================

For more information see [kwlug.org](http://kwlug.org).

## Kanban Project Board

Project management board with Agile/Kanban.
[https://gitlab.com/kwlug/kwlug/](https://gitlab.com/kwlug/kwlug/boards?=)

Feel free to create issues as you see fit if it's something you want create it. If it's something you'll solve do it.  Just let us know it's going to get done and when you finish it.